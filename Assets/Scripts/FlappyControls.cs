﻿using UnityEngine;
using System.Collections;

public class FlappyControls : MonoBehaviour {

	public int speed = 3;
	
	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
			this.GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
			this.GetComponent<Rigidbody>().AddForce(new Vector3(0, speed, 0), ForceMode.VelocityChange);
		}
	}

	void OnTriggerEnter(Collider obj) {
		if (obj.gameObject.tag == "pipecleared") {
			GUI_Counter.counter++;
		}
		if (obj.gameObject.tag == "pipe" || obj.gameObject.tag == "ground") {
			GUI_Counter.counter=0;
			Application.LoadLevel(0);
		}
	}
}
