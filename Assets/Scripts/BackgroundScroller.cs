﻿using UnityEngine;
using System.Collections;

public class BackgroundScroller : MonoBehaviour {

	public float offset = 0;
	public float speed = -0.025f;

	// Update is called once per frame
	void Update () {
		offset += speed * Time.deltaTime;

		if (offset < -1)	offset += 1; //This keeps the value between 0 and 1 

		this.GetComponent<Renderer>().material.mainTextureOffset = new Vector2 (offset, 0);
	}
}
