﻿using UnityEngine;
using System.Collections;

public class PipesMover : MonoBehaviour {

	public int speed = 5;
	
	// Update is called once per frame
	void Update () {
		this.transform.Translate (-Vector3.right * speed * Time.deltaTime);

		//remove object if off screen to left
		if (this.transform.position.x < -8)
			Destroy (this.gameObject);
	}
}
