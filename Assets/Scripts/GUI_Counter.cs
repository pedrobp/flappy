﻿using UnityEngine;
using System.Collections;

public class GUI_Counter : MonoBehaviour {

	public GUIStyle myStyle;
	public static int counter = 0;

	// Use this for initialization
	void OnGUI() {
		GUI.Label (new Rect (Screen.width - 75, 50, 50, 50), "" + counter, myStyle);
	}
}
