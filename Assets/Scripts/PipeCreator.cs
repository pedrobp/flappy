﻿using UnityEngine;
using System.Collections;

public class PipeCreator : MonoBehaviour {

	public GameObject pipes;

	// Use this for initialization
	void Start () {
		InvokeRepeating ("createNewPipeSet", 2.0f, 2.0f);
	}
	
	// Update is called once per frame
	void createNewPipeSet () {
		float ypos = Random.Range (-2.5f, 1.5f);
		GameObject obj = Instantiate (pipes,
		                              new Vector3 (8, ypos, -1),
		                              Quaternion.identity) as GameObject;
	}
}
